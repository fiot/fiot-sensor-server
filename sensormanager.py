import os
import sys
import importlib
import inspect

class SensorManager():
    def __init__(self, directory=None):
        self.sensors = {}
        if directory != None:
            self.addSensorsFromDirectory(directory)

    def getReading(self, sensor):
        return self.sensors[sensor].getReading()

    def getAllSensors(self):
        return list(self.sensors.keys())

    def addSensor(self, name, handler):
        s = handler()
        self.sensors[name] = s

    def addSensorsFromDirectory(self, directory):
        sys.path.append(os.path.join(os.getcwd(), directory))
        for sensor in [f for f in os.listdir(directory) if f.endswith('.py')]:
            name = sensor.replace('.py', '')
            module = importlib.import_module(name)
            handler = [t for t in dir(module) if not t.endswith('__')][0]
            self.addSensor(name, getattr(module, handler))
