# fiot-sensor-server
A nodejs - based server on top of python-based sensors

Acknowledgements:

- Written by Parker Woodworth
- with some additional fiddling by me
- code source: https://gitlab.com/fiot/fiot-sensor-server

Use cases:

- you have some python drivers for sensors, connected to a raspberry pi
- you want to access sensor data via http (useful for bringing the sensor data into a browser, or accessing it remotely)
- (you could also do something similar with Flask)

# prequisites 

## git installed
- sudo apt-get install git
 
## install latest version of node
- don't use the standard ubuntu repository version (it's too old)
- https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-16-04)

## install pip 
- sudo apt-get install pip

## install virtualenv 
- pip install virtualenv
- http://docs.python-guide.org/en/latest/dev/virtualenvs/

# installation procedure

## git clone the repo
- review the folder structure
- review the index.js code

##  create a virtualenv

virtualenv venv  

## activate the virtualenv

source venv/bin/activate

## install pyserial

pip install pyserial

## install the node module

npm install

## identify your local IP

hostname -I

## run the server

node index.js
- or
DEBUG=pythonic node index.js

## navigate to [yourip]:3000/sensors/list
- [yoyurip]:3000/sensors/list
- [yourip]:3000/sensors/all
- [yourip]:3000/sensors/temp

## experiment
- change the timing on the sensor
- change the output of python
