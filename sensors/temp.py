import random
import time

class TempSensor():
    def getReading(self):
        time.sleep(1)
        return random.randint(50, 120)
