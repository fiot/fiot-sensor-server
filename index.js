'use strict';

var os = require('os');
var ifaces = os.networkInterfaces();


const Pythonic = require('pythonic');
const express = require('express');

const app = express()

const py = new Pythonic([{
  dir: '.',
  name: 'sensormanager',
  init: [
    {
      class: 'SensorManager',
      as: 's',
      args: [],
      kwargs: {directory: './sensors'},
    },
  ],
}]);


app.get('/all', (req, res) => {
  py.run.s.getAllSensors().then(allSensors => {
    Promise.all(
      allSensors.map((sensor) => py.run.s.getReading([sensor]))
    ).then(allSensorData => {
      res.json(
        allSensorData.reduce((result, sensorData, index) => {
          return Object.assign({}, result, {[allSensors[index]]: sensorData})
        }, {})
      )
    })
  })
})


app.get('/list', (req, res) => {
  py.run.s.getAllSensors().then( list => {
    res.json(list)
  })
})


app.get('/sensors/:sensor', (req, res) => {
  py.run.s.getReading([req.params.sensor]).then(data => {
    res.json(data)
  })
})

py.onReady(()=>{
  app.listen(3000, () => console.log('Example app listening on port 3000!'))
})


// get IP address and print it out

Object.keys(ifaces).forEach(function (ifname) {
  var alias = 0;

  ifaces[ifname].forEach(function (iface) {
    if ('IPv4' !== iface.family || iface.internal !== false) {
      // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
      return;
    }

    if (alias >= 1) {
      // this single interface has multiple ipv4 addresses
      console.log(ifname + ':' + alias, iface.address);
    } else {
      // this interface has only one ipv4 adress
      console.log(ifname, iface.address);
    }
    ++alias;
  });
});


