[Video tutorial](https://www.youtube.com/watch?v=vArGekEaYQI)

# Set up on Debian / Ubuntu:

Here is an (overlong) [video tutorial](Video tutorial](https://www.youtube.com/watch?v=vArGekEaYQI)



### get the latest version of node

`sudo bash nodesource_setup.sh
curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh
sudo apt-get install nodejs
`

###  set up virtuenv

(following reference [here](http://docs.python-guide.org/en/latest/dev/virtualenvs/)):

`pip install virtualenv
virtualenv --version
virtualenv my_project
source my_project/bin/activate`

###  get the repo and install

`git clone https://gitlab.com/fiot/fiot-sensor-server.git
cd fiot-sensor-server
npm install
`

###  run

`node index.js`

Then you can navigate to:
localIPaddress:3000/all

###  debugging

For debugging python code, run with:

`DEBUG=pythonic node index.js`

